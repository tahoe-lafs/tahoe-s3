# Tahoe-S3

[![Most recent pipeline status badge](https://gitlab.com/tahoe-lafs/tahoe-s3/badges/main/pipeline.svg)](https://gitlab.com/tahoe-lafs/tahoe-s3/-/pipelines?scope=all&ref=main)

## What is it?

Tahoe-S3 is a Tahoe-LAFS server implementation backed by Amazon Web Service's S3 file system.

## How do I make it go?

Running tests:

    $ nix run .#cabal-test
